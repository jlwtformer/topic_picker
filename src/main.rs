use std::io;
use std::convert::TryFrom;
use rand::Rng;

fn main() {
	let count = get_count();
	let topics = get_topics(count);
	let selected_index: usize = pick_random(count);
	
	println!("\nYou should pick the topic {}", topics[selected_index]);
}

fn get_count() -> u32 {
	let mut x = String::new();
	
	println!("How many topics will you need to chose from?");
	io::stdin()
		.read_line(&mut x)
		.expect("Error reading from stdin.");
		
	let x: u32 = x.trim().parse().expect("Error parsing input.");
	
	x
}

fn get_topics(count: u32) -> Vec<String> {
	let mut topics: Vec<String> = Vec::new();
	
	for _i in 0..count {
		let mut input = String::new();
		println!("Please input a topic:");
		
		io::stdin()
			.read_line(&mut input)
			.expect("Error reading from stdin.");
		
		topics.push(input.to_string());
	}
	
	topics
}

fn pick_random(x: u32) -> usize {
	let rand_int = rand::thread_rng().gen_range(0..x);
	usize::try_from(rand_int).expect("Error converting u32 to usize.")
}
